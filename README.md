# prom-elastic

K8S monitoring setup using prometheus + elasticsearch + metricbeat + kibana

## Prereqs
- K8S cluster
  - [K8S Getting started](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/)
- Prometheus Operator
  - [Prometheus Operator Quickstart](https://prometheus-operator.dev/docs/prologue/quick-start/)
- Ingress Controller
  - [K8S Ingress controllers](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
- ECK operator
  - [ECK Quickstart](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html)


